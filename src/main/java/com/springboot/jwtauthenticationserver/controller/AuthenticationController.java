package com.springboot.jwtauthenticationserver.controller;

import com.springboot.jwtauthenticationserver.domain.dao.UserRepository;
import com.springboot.jwtauthenticationserver.payload.ApiResponse;
import com.springboot.jwtauthenticationserver.payload.LoginRequest;
import com.springboot.jwtauthenticationserver.payload.RegisterRequest;
import com.springboot.jwtauthenticationserver.security.JwtTokenUtility;
import com.springboot.jwtauthenticationserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtTokenUtility tokenProvider;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);

        HttpHeaders responseHeaders = new HttpHeaders();

        responseHeaders.add("Authorization: ", "Bearer " + jwt);

        return new ResponseEntity<>(
                new ApiResponse(true, "User was successfully authenticated"),
                responseHeaders, HttpStatus.OK);
    }

    @PostMapping(value = "/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody RegisterRequest registerRequest) {
        if(userRepository.existsByUsername(registerRequest.getUsername())) {
            return new ResponseEntity<>(
                    new ApiResponse(false, "User already exists!"),
                    HttpStatus.BAD_REQUEST);
        }

        if(userRepository.existsByEmail(registerRequest.getEmail())) {
            return new ResponseEntity<>(
                    new ApiResponse(false, "Email already in use"),
                    HttpStatus.BAD_REQUEST
            );
        }

        if(userService.createNewUser(registerRequest))
            return ResponseEntity.ok(new ApiResponse(true, "User was created"));
        else
            return new ResponseEntity<>(
                    new ApiResponse(false, "User wasn't created"),
                    HttpStatus.BAD_REQUEST
            );
    }


}
