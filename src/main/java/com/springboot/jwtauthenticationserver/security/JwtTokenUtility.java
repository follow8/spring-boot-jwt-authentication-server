package com.springboot.jwtauthenticationserver.security;

import com.springboot.jwtauthenticationserver.domain.model.User;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtTokenUtility {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenUtility.class);

    private static final Long jwtValidityPeriod = (long)(3600 * 72 * 1000);

    @Value("${app.jwt_secret}")
    private String jwtSecret;


    public String generateToken(Authentication authentication) {

        User user = (User) authentication.getPrincipal();

        Date expiryDate = new Date(new Date().getTime() + jwtValidityPeriod);

        return Jwts.builder()
                .setSubject(Long.toString(user.getId()))
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS256, jwtSecret)
                .compact();

    }

    public Claims getClaimsBody(String token) {
        return Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();
    }

    public Date getExpiryDateFromJwt(String token) {
        return getClaimsBody(token).getExpiration();
    }


    public Long getUserIdFromJwt(String token) {
        return Long.parseLong(
                getClaimsBody(token)
                        .getSubject());
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser()
                    .setSigningKey(jwtSecret)
                    .requireExpiration(getExpiryDateFromJwt(token))
                    .parseClaimsJws(token);

            return true;
        }catch (SignatureException | ExpiredJwtException
                | UnsupportedJwtException | IllegalArgumentException ex) {
            logger.error("SOMETHING WRONG WITH VALIDATION OF TOKEN");
        }

        return false;
    }
}
